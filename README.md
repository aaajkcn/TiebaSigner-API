# 贴吧云签到API
## 用于与外部程序譬如客户端或者签到联盟进行对接

API接口的详细用法请看Wiki：[点此进入](https://git.oschina.net/fsgmhoward/TiebaSigner-API/wikis/API%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3)

## BUG汇报
* 假如是安全类的BUG请直接发邮件至howard#ixnet.work（把#换成@）。
* 非安全类的问题请直接提交issue。

## 开源协议
MIT 开源，详见LICENSE文件。