<?php

if (!defined('SYSTEM_ROOT')) {
    die('Insufficient Permissions');
}

require 'ixnet_tsapi.php';

/**
 * 安装的时候创建数据库并生成管理员API密匙
 */
function callback_install()
{
    global $m;
    // 创建保存API密匙的数据表
    $m->query("
    CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ixnet_tsapi_keys` (
        `uid` INT(255)   NOT NULL,
        `key` BINARY(64) NOT NULL,
        PRIMARY KEY (`uid`)
    )
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
    ");

    // 创建保存登陆Token的表
    $m->query("
    CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ixnet_tsapi_tokens` (
        `uid`       INT(255)   NOT NULL,
        `token`     BINARY(64) NOT NULL,
        `create_at` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`uid`)
    )
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
    ");
    
    // 创建记录IP错误次数的数据表以防爆破
    $m->query("
    CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ixnet_tsapi_firewall` (
        `ip`       VARCHAR(39) NOT NULL,
        `times`    INT(255)    NOT NULL DEFAULT 1,
        `last_try` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`ip`)
    )
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
    ");

    // 为每一位管理员生成API密匙（可控制整个站）
    $admins = $m->query('SELECT `id` FROM `'.DB_PREFIX.'users` WHERE `role`=\'admin\';');
    while ($admin = $m->fetch_array($admins)) {
        ixnet_tsapi_resetKey($admin['id']);
    }

    // 创建配置：一小时内最大尝试次数，默认为10次
    option::add('ixnet_tsapi_max_try', 10);
}

/**
 * 卸载的时候删除数据库
 */
function callback_remove()
{
    global $m;
    $m->query('DROP TABLE IF EXISTS `'.DB_PREFIX.'ixnet_tsapi_keys`');
    $m->query('DROP TABLE IF EXISTS `'.DB_PREFIX.'ixnet_tsapi_tokens`');
    $m->query('DROP TABLE IF EXISTS `'.DB_PREFIX.'ixnet_tsapi_firewall`');
    option::del('ixnet_tsapi_max_try');
}

/**
 * 激活的时候添加Cron脚本
 */
function callback_init()
{
    cron::set('ixnet_tsapi', 'plugins/ixnet_tsapi/ixnet_tsapi_cron.php', 0, '云签到API计划任务，用于定时清理防火墙和登陆Token', 0);
}

/**
 * 禁用的时候取消Cron脚本
 */
function callback_inactive()
{
    cron::del('ixnet_tsapi');
}
