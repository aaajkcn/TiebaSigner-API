<?php

if (!defined('SYSTEM_ROOT')) {
    die('Insufficient Permissions');
}

/**
 * 生成一个64位的包含大小写字母的随机字符串
 *
 * @return string
 */
function ixnet_tsapi_genKey()
{
    $output='';
    while (strlen($output) < 64) {
        // ASCII: A->Z = 65->90; a->z = 97->122
        $output.=chr(mt_rand(0, 1) ? mt_rand(65, 90) : mt_rand(97, 122));
    }
    return $output;
}

/**
 * 为某个指定的用户创建或者重置API密匙
 *
 * @param int $uid
 * @return string
 * @throws Exception
 */
function ixnet_tsapi_resetKey($uid)
{
    global $m;
    $key = ixnet_tsapi_genKey();
    $m->query("
    INSERT INTO `".DB_PREFIX."ixnet_tsapi_keys` (`uid`, `key`) VALUES ('$uid', '$key')
        ON DUPLICATE KEY UPDATE `key` = '$key';
    ");
    return $key;
}

/**
 * 获取一个用户的密匙，假如密匙不存在则返回false。
 *
 * @param int $uid
 * @return bool|string
 */
function ixnet_tsapi_getKey($uid)
{
    global $m;
    $result = $m->once_fetch_array('SELECT `key` FROM `'.DB_PREFIX.'ixnet_tsapi_keys` WHERE `uid`=\''.$uid.'\';');
    if ($result) {
        return $result['key'];
    } else {
        return false;
    }
}

/**
 * 在导航边栏和顶栏插入API设置的链接
 */
function ixnet_tsapi_navi()
{
    echo '<li';
    if (isset($_GET['plugin']) && $_GET['plugin'] == 'ixnet_tsapi') {
        echo ' class="active"';
    }
    echo '><a href="index.php?plugin=ixnet_tsapi"><span class="glyphicon glyphicon-send"></span> 云签到API</a></li>';
}

addAction('navi_3', 'ixnet_tsapi_navi');
addAction('navi_9', 'ixnet_tsapi_navi');
