<?php

if (!defined('SYSTEM_ROOT')) {
    die('Insufficient Permissions');
}

loadhead('云签到API配置');
if (isset($_GET['genKey'])) {
    if (ROLE == 'admin' && isset($_GET['target'])) {
        $key = ixnet_tsapi_resetKey($_GET['target']);
        header('Location: ?plugin=ixnet_tsapi&userKey='.$key);
    } else {
        ixnet_tsapi_resetKey($_COOKIE['uid']);
        header('Location: ?plugin=ixnet_tsapi');
    }
    exit;
} else {
    $key = ixnet_tsapi_getKey($_COOKIE['uid']);
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">API密匙设置</div>
            <div class="panel-body">
                <?php
                if ($key) {
                    ?>
                    <p>你的uid是：<span class="text-info"><?php echo $_COOKIE['uid'];?></span></p>
                    <p>你当前的密匙是：<span class="text-info"><?php echo $key;?></span></p>
                    <p>你的权限级别是：<span class="text-info"><?php echo ROLE;?></span></p>
                    <p><a role="button" class="btn btn-default" href="?plugin=ixnet_tsapi&genKey=true">重置API密匙</a></p>
                    <p class="text-danger">警告：重置密匙要谨慎，你所有关联的软件都需要更新密匙，并有可能导致你无法登陆云签。</p>
                <?php
                } else {
                    ?>
                    <p>你当前没有配置密匙，点击下面的按钮生成一个密匙</p>
                    <p><a role="button" class="btn btn-default" href="?plugin=ixnet_tsapi&genKey=true">生成API密匙</a></p>
                <?php
                }
                if (ROLE == 'admin') {
                    ?>
                    <hr size="2px">
                    <p>为一个用户重置API密匙</p>
<!--                    TODO: 重写为AJAX，把管理员重置用户API密匙功能加入API -->
                    <form class="form-inline">
                        <input type="text" name="plugin" value="ixnet_tsapi" placeholder="" hidden>
                        <input type="text" name="genKey" value="true" placeholder="" hidden>
                        <div class="form-group">
                            <p>目标用户UID：<input class="form-control" type="text" name="target" placeholder="应为一个正整数">&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-default" role="button">点击重置</button></p>
                        </div>
                    </form>
                    <?php
                    if (isset($_GET['userKey'])) {
                        echo '<p class="text-info">成功重置用户密匙，新密匙为：'.$_GET['userKey'];
                    }
                } ?>
                <hr size="2px">
                <p class="text-muted">&copy; 云签到API by <a href="https://www.ixnet.work">IX Network Studio</a></p>
            </div>
        </div>
    </div>
</div>
<?php
loadfoot();
?>