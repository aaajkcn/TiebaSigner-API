<?php
/**
 * IXNetwork云签到API公开指令
 * 这个文件下所有指令都只需要action即可执行
 */
if (!defined('SYSTEM_ROOT')) {
    exit('Insufficient Permission');
}
global $m, $i;
switch ($action) {
    // 返回服务器状态
    case 'info':
        // 这一段代码拷贝自/templates/admin-stat.php
        $day = date('d');
        $statusUsers = $m->query("SELECT * FROM `".DB_PREFIX."users` ORDER BY `id`");
        $statusBDUSSNumber = $m->once_fetch_array("SELECT COUNT(*) AS `c` FROM `".DB_PREFIX."baiduid`");
        $statusSuccess = $statusError = $statusIgnored = $statusTotal = $statusWaiting = 0;
        while ($statusUser = $m->fetch_array($statusUsers)) {
            $list = $m->query("SELECT `no`,`status`,`latest` FROM `".DB_PREFIX.$statusUser['t']."` WHERE `uid` = ".$statusUser['id']);
            $success = $error = $ignored = $waiting = 0;
            $total = $m->num_rows($list);
            while ($x = $m->fetch_array($list)) {
                if ($x['no'] == '1') {
                    $ignored++;
                } elseif ($x['latest'] != $day) {
                    $waiting++;
                } elseif ($x['status'] == '0') {
                    $success++;
                } elseif ($x['status'] != '0') {
                    $error++;
                }
            }
            $statusWaiting = $statusWaiting + $waiting;
            $statusSuccess = $statusSuccess + $success;
            $statusIgnored = $statusIgnored + $ignored;
            $statusTotal = $statusTotal + $total;
            $statusError = $statusError + $error;
        }

        // 获取用户总数
        $statusUsersNumber = $m->once_fetch_array('SELECT COUNT(*) AS c FROM `'.DB_PREFIX.'users`;');
        // 获取插件版本
        $pluginDesc = require "ixnet_tsapi_desc.php";

        ixnet_tsapi_return(array(
            'info' => array(
                'core_version'  => option::get('core_version'),
                'core_revision' => option::get('core_revision'),
                'api_version'   => $pluginDesc['plugin']['version'],
                'users'    => $statusUsersNumber['c'],
                'bduss'    => $statusBDUSSNumber['c'],
                'icp'      => option::get('icp'),
                'name'     => option::get('system_name'),
                'url'      => option::get('system_url'),
                'plugins'  => $i['plugins']['all']
            ),
            'sign' => array(
                'waiting' => $statusWaiting,
                'success' => $statusSuccess,
                'error'   => $statusError,
                'ignored' => $statusIgnored,
                'total'   => $statusTotal
            ),
            'constraint' => array(
                'reg'      => option::get('enable_reg'),
                'addtieba' => option::get('enable_addtieba'),
                'samepid'  => option::get('same_pid'),
                'bduss'    => option::get('bduss_num'),
                'tieba'    => option::get('tb_max')
            )
        ));
        break;
    
    default:
        // 出现未知错误
        ixnet_tsapi_return('Unknown Internal Error', 500);
}