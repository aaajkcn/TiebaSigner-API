<?php
global $m, $uid, $target;
$actionPublic = array('info');
$actionUser = array('login', 'pre_login', 'bduss_add', 'bduss_del', 'bduss_get', 'api_reset');
$actionAdmin = array('list', 'register', 'remove', 'hash', 'remote', 'bduss');

/**
 * 测试是否为管理员，不是则终止程序
 */
function ixnet_tsapi_require_admin()
{
    global $role;
    if ($role != 'admin') {
        ixnet_tsapi_return('Insufficient Permissions', 403);
    }
}

/**
 * 以JSON的格式返回数据。当状态码不为200时$data应为字符串类型的错误信息。
 *
 * @param array|string $data
 * @param int $code
 */
function ixnet_tsapi_return($data = [], $code = 200)
{
    // 假如状态码为403（拒绝访问）则记录IP
    if ($code == 403) {
        global $m;
        $m->query("
        INSERT INTO `".DB_PREFIX."ixnet_tsapi_firewall` (`ip`) VALUES ('".getIp()."')
            ON DUPLICATE KEY UPDATE `times`=`times`+1;
        ");
    }

    header('HTTP/1.1 '.$code.' '.($code == 200 ? 'OK' : $data));
    header('Content-Type: application/json');
    $return['code'] = $code;
    if ($data) {
        $return['result'] = $data;
    }
    exit(json_encode($return));
}

// 检查是否通过正确的接口访问，参数action是否存在以及IP是否已经被拉黑
if (!defined('SYSTEM_ROOT')) {
    ixnet_tsapi_return('Insufficient Permissions', 403);
} elseif (!isset($_REQUEST['action'])) {
    ixnet_tsapi_return('Parameters Missing', 400);
} elseif ($m->once_fetch_array('SELECT * FROM `'.DB_PREFIX.'ixnet_tsapi_firewall` WHERE `ip`=\''.getIp().'\' AND `times`>=\''.option::get('ixnet_tsapi_max_try').'\';')) {
    ixnet_tsapi_return('IP Blocked', 403);
}
$action = $_REQUEST['action'];

// 判断指令应该要用到哪个控制器
if (in_array($action, $actionPublic)) {
    // 公共指令，除了action以外不需要提供任何参数
    require 'action_public.php';
} else {
    // 检查uid和密匙，并获取用户信息
    if (!isset($_REQUEST['uid']) || !isset($_REQUEST['key'])) {
        ixnet_tsapi_return('Parameters Missing', 400);
        // 防注入检测
    } elseif (preg_match('/[^a-zA-Z]/', $_REQUEST['key'])) {
        ixnet_tsapi_return('Do you think you can hack in this API? Too young, too simple, and sometimes naive!', 403);
        // 假如是登陆就校验token，否则校验key
    } elseif (($action=='login' && !$m->once_fetch_array('SELECT * FROM `'.DB_PREFIX.'ixnet_tsapi_tokens` WHERE `uid`=\''.$_REQUEST['uid'].'\' AND `token`=\''.$_REQUEST['key'].'\';')) ||
        ($action!='login' && !$m->once_fetch_array('SELECT * FROM `'.DB_PREFIX.'ixnet_tsapi_keys` WHERE `uid`=\''.$_REQUEST['uid'].'\' AND `key`=\''.$_REQUEST['key'].'\';'))) {
        ixnet_tsapi_return('Invalid UID or Key', 403);
    }
    $uid  = $_REQUEST['uid'];
    $user = $m->once_fetch_array('SELECT * FROM `'.DB_PREFIX.'users` WHERE `id`=\''.$uid.'\';');
    $role = $user['role'];
    
    // 只有管理员才允许制定目标用户，假如出现target参数但是却不是管理员，程序将会终止
    if (isset($_REQUEST['target'])) {
        ixnet_tsapi_require_admin();
        $target = $_REQUEST['target'];
    } else {
        $target = $_REQUEST['uid'];
    }
    if (in_array($action, $actionUser)) {
        // 普通用户也可以执行的指令
        require 'action_user.php';
    } elseif (in_array($action, $actionAdmin)) {
        // 管理员才可以执行的指令
        ixnet_tsapi_require_admin();
        require 'action_admin.php';
    } else {
        // 没找到所需的指令
        ixnet_tsapi_return('Action Not Found', 404);
    }
}
