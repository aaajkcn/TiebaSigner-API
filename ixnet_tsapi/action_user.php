<?php
/**
 * IXNetwork云签到API用户指令
 * 这一文件里面的指令除了action参数以外还需要key参数和uid参数，但是密匙不一定要属于管理员。
 */
if (!defined('SYSTEM_ROOT')) {
    exit('Insufficient Permission');
}
global $m, $uid, $target;
switch ($action) {
    // 用户通过Token进行Web登陆
    case 'login':
        // 销毁Token
        $m->query('DELETE FROM `' . DB_PREFIX . 'ixnet_tsapi_tokens` WHERE `token`=\'' . $_REQUEST['key'] . '\';');
        $cktime = intval(option::get('cktime'));
        setcookie('uid', $uid, time() + $cktime);
        setcookie('pwd', substr(sha1(EncodePwd($user['pw'])), 4, 32), time() + $cktime);
        header('Location: index.php');
        break;

    // 获取登录Token，也允许管理员获取别的用户的登陆token
    case 'pre_login':
        // 允许一些程序给获取登录token添加限制
        doAction('ixnet_tsapi_pre_login');
        $token = ixnet_tsapi_genKey();
        $m->query("
        INSERT INTO `" . DB_PREFIX . "ixnet_tsapi_tokens` (`uid`,`token`) VALUES ('$target', '$token')
            ON DUPLICATE KEY UPDATE `token`='$token';
        ");
        ixnet_tsapi_return(array(
            'target' => $target,
            'token' => $token
        ));
        break;

    // 添加一个BDUSS，也允许管理员添加别的用户的BDUSS
    case 'bduss_add':
        if (!isset($_REQUEST['bduss'])) {
            ixnet_tsapi_return('Parameters Missing', 400);
        }
        $m->query('INSERT INTO `' . DB_PREFIX . 'baiduid` (`uid`, `bduss`) VALUES (\'' . $target . '\', \'' . $_REQUEST['bduss'] . '\');');
        $id = $m->insert_id();
        ixnet_tsapi_return(array(
            'target' => $target,
            'bduss' => $_REQUEST['bduss'],
            'id' => $id
        ));
        break;

    // 删除一个BDUSS，也允许管理员删除别的用户的BDUSS
    case 'bduss_del':
        if (!isset($_REQUEST['bduss'])) {
            ixnet_tsapi_return('Parameters Missing', 400);
        }
        $m->query('DELETE FROM `' . DB_PREFIX . 'baiduid` WHERE `bduss`=\'' . $_REQUEST['bduss'] . '\' AND `uid`=\'' . $target . '\';');
        ixnet_tsapi_return(array(
            'target' => $target,
            'bduss' => $_REQUEST['bduss']
        ));
        break;

    // 获取当前用户的所有BDUSS，或获取指定用户的BDUSS（需要管理员权限）
    case 'bduss_get':
        $result = $m->query('SELECT `id`,`bduss` FROM `' . DB_PREFIX . 'baiduid` WHERE `uid`=\'' . $target . '\' ORDER BY `id` ASC;');
        $return = [];
        while ($bduss = $m->fetch_array($result)) {
            $return[$bduss['id']] = $bduss['bduss'];
        }
        ixnet_tsapi_return(array(
            'target' => $target,
            'bduss' => $return
        ));
        break;

    // 重置API密匙，也可以重置某个用户的API密匙（需要管理员权限）
    case 'api_reset':
        ixnet_tsapi_return(array(
            'target' => $target,
            'key' => ixnet_tsapi_resetKey($target)
        ));
        break;

    default:
        // 出现未知错误
        ixnet_tsapi_return('Unknown Internal Error', 500);
}
