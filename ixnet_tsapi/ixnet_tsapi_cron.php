<?php

global $m;

if (!defined('SYSTEM_ROOT')) {
    die('Insufficient Permissions');
}

// 清理一小时前的记录
$m->query('DELETE FROM `'.DB_PREFIX.'ixnet_tsapi_firewall` WHERE `last_try`<NOW()-10000;');
// 清理五分钟之前创建的Token
$m->query('DELETE FROM `'.DB_PREFIX.'ixnet_tsapi_tokens` WHERE `create_at`<NOW()-500;');
